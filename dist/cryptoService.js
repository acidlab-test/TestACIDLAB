'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.CURRENCY = exports.LastDaySchedule = exports.LastYearSchedule = exports.LastYear = exports.LastDay = undefined;

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _axios = require('axios');

var _axios2 = _interopRequireDefault(_axios);

var _redis = require('redis');

var _redis2 = _interopRequireDefault(_redis);

var _moment = require('moment');

var _moment2 = _interopRequireDefault(_moment);

var _nodeSchedule = require('node-schedule');

var _nodeSchedule2 = _interopRequireDefault(_nodeSchedule);

var _babelPolyfill = require('babel-polyfill');

var _babelPolyfill2 = _interopRequireDefault(_babelPolyfill);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

var KEY = 'KGLVLPHYKV2HP5SR';

var CURRENCY = ["BTC", "ETH"];

var client = _redis2.default.createClient();

client.on("error", function (err) {
    console.log("Error " + err);
});

_nodeSchedule2.default.scheduleJob('LastDaySchedule', '0 0 * * * *', _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
    var error;
    return regeneratorRuntime.wrap(function _callee$(_context) {
        while (1) {
            switch (_context.prev = _context.next) {
                case 0:
                    error = void 0;

                case 1:
                    error = Math.random(0, 1) < 0.1;

                    if (!error) {
                        _context.next = 6;
                        break;
                    }

                    console.log("Simulate Success LastDaySchedule");
                    _context.next = 6;
                    return LastDaySchedule();

                case 6:
                    if (!error) {
                        _context.next = 1;
                        break;
                    }

                case 7:
                case 'end':
                    return _context.stop();
            }
        }
    }, _callee, undefined);
})));

_nodeSchedule2.default.scheduleJob('LastYearSchedule', '0 0 1 * * *', _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
    var error;
    return regeneratorRuntime.wrap(function _callee2$(_context2) {
        while (1) {
            switch (_context2.prev = _context2.next) {
                case 0:
                    error = void 0;

                case 1:
                    error = Math.random(0, 1) < 0.1;

                    if (!error) {
                        _context2.next = 8;
                        break;
                    }

                    console.log("Simulate Success LastYearSchedule");
                    _context2.next = 6;
                    return LastYearSchedule();

                case 6:
                    _context2.next = 9;
                    break;

                case 8:
                    console.log("intentar LastYearSchedule");

                case 9:
                    if (!error) {
                        _context2.next = 1;
                        break;
                    }

                case 10:
                case 'end':
                    return _context2.stop();
            }
        }
    }, _callee2, undefined);
})));

var LastDaySchedule = function () {
    var _ref3 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee5() {
        return regeneratorRuntime.wrap(function _callee5$(_context5) {
            while (1) {
                switch (_context5.prev = _context5.next) {
                    case 0:
                        CURRENCY.map(function () {
                            var _ref4 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee4(currency) {
                                var url, _hours;

                                return regeneratorRuntime.wrap(function _callee4$(_context4) {
                                    while (1) {
                                        switch (_context4.prev = _context4.next) {
                                            case 0:
                                                _context4.prev = 0;
                                                url = 'https://www.alphavantage.co/query?function=DIGITAL_CURRENCY_INTRADAY&symbol=' + currency + '&market=USD&apikey=' + KEY;
                                                _context4.next = 4;
                                                return _axios2.default.get(url).then(function (_ref5) {
                                                    var data = _ref5.data;

                                                    var dataFetch = data;

                                                    var _Object$keys = Object.keys(dataFetch),
                                                        _Object$keys2 = _slicedToArray(_Object$keys, 2),
                                                        meta = _Object$keys2[0],
                                                        description = _Object$keys2[1];

                                                    if (!description) {
                                                        console.log({ currency: currency, error: true });
                                                        return false;
                                                    }
                                                    var hours = Object.keys(dataFetch[description]).filter(function (each) {
                                                        var now = (0, _moment2.default)();
                                                        var year = now.get('year'),
                                                            month = now.get('month') + 1;
                                                        return each.includes(year + '-' + month);
                                                    }).filter(function (eachone) {
                                                        return eachone.includes("00:00");
                                                    }).slice(0, 24).map(function (eachtwo, keytwo) {
                                                        var _Object$keys3 = Object.keys(dataFetch[description][eachtwo]),
                                                            _Object$keys4 = _slicedToArray(_Object$keys3, 1),
                                                            price = _Object$keys4[0];

                                                        var priceValue = dataFetch[description][eachtwo][price];
                                                        return { date: eachtwo, price: priceValue, currency: currency, keytwo: keytwo };
                                                    });
                                                    return hours;
                                                });

                                            case 4:
                                                _hours = _context4.sent;

                                                if (!_hours) {
                                                    console.log("nothing to do - repeat Fetch");
                                                    LastDaySchedule();
                                                } else {
                                                    _hours.map(function (value, key) {

                                                        var nextDay = _moment2.default.utc(value.date).add(1, 'day');

                                                        var currentDate = (0, _moment2.default)().minute(0).seconds(0).millisecond(0);

                                                        var expire = nextDay.diff(currentDate) / 1000;
                                                        value.expire = expire;
                                                        return value;
                                                    }).filter(function (valid) {
                                                        return valid;
                                                    }).map(function () {
                                                        var _ref6 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3(value, k) {
                                                            var diff, positive, dateToSave, toSave, toSaveString;
                                                            return regeneratorRuntime.wrap(function _callee3$(_context3) {
                                                                while (1) {
                                                                    switch (_context3.prev = _context3.next) {
                                                                        case 0:
                                                                            _context3.prev = 0;
                                                                            diff = parseInt((0, _moment2.default)().format("Z").split(':')[0]);
                                                                            positive = diff > 0;
                                                                            dateToSave = positive ? (0, _moment2.default)(value.date).add(Math.abs(diff), 'hour').format("YYYY-MM-DD HH:mm:ss") : (0, _moment2.default)(value.date).subtract(Math.abs(diff), 'hour').format("YYYY-MM-DD HH:mm:ss");

                                                                            if (value.price) {
                                                                                _context3.next = 8;
                                                                                break;
                                                                            }

                                                                            console.log({ value: value });
                                                                            _context3.next = 13;
                                                                            break;

                                                                        case 8:
                                                                            toSave = {
                                                                                price: value.price,
                                                                                date: dateToSave
                                                                            };
                                                                            _context3.next = 11;
                                                                            return JSON.stringify(toSave);

                                                                        case 11:
                                                                            toSaveString = _context3.sent;

                                                                            client.exists(currency + '-' + dateToSave, function (err, reply) {
                                                                                if (reply === 0) {
                                                                                    if (value.expire > 0) {
                                                                                        client.set(currency + '-' + dateToSave, toSaveString, 'EX', value.expire);
                                                                                    }
                                                                                }
                                                                            });

                                                                        case 13:
                                                                            _context3.next = 19;
                                                                            break;

                                                                        case 15:
                                                                            _context3.prev = 15;
                                                                            _context3.t0 = _context3['catch'](0);

                                                                            console.log('Error en el Exist - ' + currency);
                                                                            console.log({ error: _context3.t0 });

                                                                        case 19:
                                                                        case 'end':
                                                                            return _context3.stop();
                                                                    }
                                                                }
                                                            }, _callee3, undefined, [[0, 15]]);
                                                        }));

                                                        return function (_x2, _x3) {
                                                            return _ref6.apply(this, arguments);
                                                        };
                                                    }());
                                                }
                                                _context4.next = 12;
                                                break;

                                            case 8:
                                                _context4.prev = 8;
                                                _context4.t0 = _context4['catch'](0);

                                                console.log("Error Day");
                                                console.log({ error: _context4.t0 });

                                            case 12:
                                            case 'end':
                                                return _context4.stop();
                                        }
                                    }
                                }, _callee4, undefined, [[0, 8]]);
                            }));

                            return function (_x) {
                                return _ref4.apply(this, arguments);
                            };
                        }());

                    case 1:
                    case 'end':
                        return _context5.stop();
                }
            }
        }, _callee5, undefined);
    }));

    return function LastDaySchedule() {
        return _ref3.apply(this, arguments);
    };
}();

var LastYearSchedule = function () {
    var _ref7 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee7() {
        return regeneratorRuntime.wrap(function _callee7$(_context7) {
            while (1) {
                switch (_context7.prev = _context7.next) {
                    case 0:
                        try {
                            CURRENCY.map(function () {
                                var _ref8 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee6(currency) {
                                    var url;
                                    return regeneratorRuntime.wrap(function _callee6$(_context6) {
                                        while (1) {
                                            switch (_context6.prev = _context6.next) {
                                                case 0:
                                                    url = 'https://www.alphavantage.co/query?function=DIGITAL_CURRENCY_MONTHLY&symbol=' + currency + '&market=USD&apikey=KGLVLPHYKV2HP5SR';
                                                    _context6.next = 3;
                                                    return _axios2.default.get(url).then(function (_ref9) {
                                                        var data = _ref9.data;

                                                        var _Object$keys5 = Object.keys(data),
                                                            _Object$keys6 = _slicedToArray(_Object$keys5, 2),
                                                            description = _Object$keys6[1];

                                                        var OneYearAgo = (0, _moment2.default)().subtract(1, 'year').format("YYYY-MM-DD");
                                                        var months = Object.keys(data[description]).filter(function (each) {
                                                            return each > OneYearAgo;
                                                        }).map(function (each, index) {
                                                            var _Object$keys7 = Object.keys(data[description][each]),
                                                                _Object$keys8 = _slicedToArray(_Object$keys7, 1),
                                                                price = _Object$keys8[0];

                                                            var priceValue = data[description][each][price];
                                                            var obj = Object.assign({}, {
                                                                date: each,
                                                                price: priceValue
                                                            });
                                                            return obj;
                                                        });
                                                        var currencyExpire = months.reverse().map(function (value, key) {
                                                            var nextMonth = _moment2.default.utc().add(key + 1, 'month');
                                                            var currentDate = _moment2.default.utc();
                                                            value.expire = Math.round(nextMonth.diff(currentDate) / (1000 * 60 * 60)) * (60 * 60 * 728);
                                                            return value;
                                                        }).filter(function (valid) {
                                                            return valid;
                                                        }).map(function (value, k) {
                                                            try {
                                                                client.exists(currency + '-' + (0, _moment2.default)(value.date).format("YYYY-MM"), function (err, reply) {
                                                                    if (reply === 0) {

                                                                        if (value.expire > 0) {
                                                                            client.set(currency + '-' + (0, _moment2.default)(value.date).format("YYYY-MM"), JSON.stringify({ price: value.price, date: value.date }), 'EX', value.expire);
                                                                        }
                                                                    }
                                                                });
                                                            } catch (error) {
                                                                console.log('Error en el Exist - ' + currency);
                                                                console.log({ error: error });
                                                            }
                                                        });
                                                    });

                                                case 3:
                                                case 'end':
                                                    return _context6.stop();
                                            }
                                        }
                                    }, _callee6, undefined);
                                }));

                                return function (_x4) {
                                    return _ref8.apply(this, arguments);
                                };
                            }());
                        } catch (error) {
                            console.log("Error Year");
                            console.log({ error: error });
                        }

                    case 1:
                    case 'end':
                        return _context7.stop();
                }
            }
        }, _callee7, undefined);
    }));

    return function LastYearSchedule() {
        return _ref7.apply(this, arguments);
    };
}();

var LastDay = function () {
    var _ref10 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee9(currency, range) {
        var promise, message;
        return regeneratorRuntime.wrap(function _callee9$(_context9) {
            while (1) {
                switch (_context9.prev = _context9.next) {
                    case 0:
                        if (!CURRENCY.some(function (coin) {
                            return coin === currency;
                        })) {
                            _context9.next = 12;
                            break;
                        }

                        promise = [];

                        [].concat(_toConsumableArray(Array(range).keys())).map(function () {
                            var _ref11 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee8(each, key) {
                                var keyDate, value;
                                return regeneratorRuntime.wrap(function _callee8$(_context8) {
                                    while (1) {
                                        switch (_context8.prev = _context8.next) {
                                            case 0:
                                                keyDate = (0, _moment2.default)().subtract(each, 'hour').format("YYYY-MM-DD HH:00:00");
                                                value = new Promise(function (resolve, reject) {
                                                    client.get(currency + '-' + keyDate, function (err, reply) {

                                                        if (!err) {
                                                            if (reply) {
                                                                resolve(JSON.parse(reply));
                                                            } else {
                                                                resolve(0);
                                                            }
                                                        } else {
                                                            resolve(0);
                                                        }
                                                    });
                                                });

                                                promise.push(value);

                                            case 3:
                                            case 'end':
                                                return _context8.stop();
                                        }
                                    }
                                }, _callee8, undefined);
                            }));

                            return function (_x7, _x8) {
                                return _ref11.apply(this, arguments);
                            };
                        }());

                        _context9.t0 = [];
                        _context9.t1 = _toConsumableArray;
                        _context9.next = 7;
                        return Promise.all(promise);

                    case 7:
                        _context9.t2 = _context9.sent;
                        _context9.t3 = (0, _context9.t1)(_context9.t2);
                        return _context9.abrupt('return', _context9.t0.concat.call(_context9.t0, _context9.t3));

                    case 12:
                        message = "Currency Invalid";
                        return _context9.abrupt('return', message);

                    case 14:
                    case 'end':
                        return _context9.stop();
                }
            }
        }, _callee9, undefined);
    }));

    return function LastDay(_x5, _x6) {
        return _ref10.apply(this, arguments);
    };
}();

var LastYear = function () {
    var _ref12 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee11(currency, range) {
        var promise, message;
        return regeneratorRuntime.wrap(function _callee11$(_context11) {
            while (1) {
                switch (_context11.prev = _context11.next) {
                    case 0:
                        if (!CURRENCY.some(function (coin) {
                            return coin === currency;
                        })) {
                            _context11.next = 12;
                            break;
                        }

                        promise = [];

                        [].concat(_toConsumableArray(Array(range).keys())).map(function () {
                            var _ref13 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee10(each, key) {
                                var keyDate, value;
                                return regeneratorRuntime.wrap(function _callee10$(_context10) {
                                    while (1) {
                                        switch (_context10.prev = _context10.next) {
                                            case 0:
                                                keyDate = (0, _moment2.default)().subtract(each, 'month').format("YYYY-MM");
                                                value = new Promise(function (resolve, reject) {
                                                    client.get(currency + '-' + keyDate, function (err, reply) {
                                                        if (!err) {
                                                            if (reply) {
                                                                resolve(JSON.parse(reply));
                                                            } else {
                                                                resolve(0);
                                                            }
                                                        } else {
                                                            resolve(0);
                                                        }
                                                    });
                                                });

                                                promise.push(value);

                                            case 3:
                                            case 'end':
                                                return _context10.stop();
                                        }
                                    }
                                }, _callee10, undefined);
                            }));

                            return function (_x11, _x12) {
                                return _ref13.apply(this, arguments);
                            };
                        }());

                        _context11.t0 = [];
                        _context11.t1 = _toConsumableArray;
                        _context11.next = 7;
                        return Promise.all(promise);

                    case 7:
                        _context11.t2 = _context11.sent;
                        _context11.t3 = (0, _context11.t1)(_context11.t2);
                        return _context11.abrupt('return', _context11.t0.concat.call(_context11.t0, _context11.t3));

                    case 12:
                        message = "Currency Invalid";
                        return _context11.abrupt('return', message);

                    case 14:
                    case 'end':
                        return _context11.stop();
                }
            }
        }, _callee11, undefined);
    }));

    return function LastYear(_x9, _x10) {
        return _ref12.apply(this, arguments);
    };
}();

exports.LastDay = LastDay;
exports.LastYear = LastYear;
exports.LastYearSchedule = LastYearSchedule;
exports.LastDaySchedule = LastDaySchedule;
exports.CURRENCY = CURRENCY;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3NyYy9jcnlwdG9TZXJ2aWNlLmpzIl0sIm5hbWVzIjpbIktFWSIsIkNVUlJFTkNZIiwiY2xpZW50IiwiY3JlYXRlQ2xpZW50Iiwib24iLCJlcnIiLCJjb25zb2xlIiwibG9nIiwic2NoZWR1bGVKb2IiLCJlcnJvciIsIk1hdGgiLCJyYW5kb20iLCJMYXN0RGF5U2NoZWR1bGUiLCJMYXN0WWVhclNjaGVkdWxlIiwibWFwIiwiY3VycmVuY3kiLCJ1cmwiLCJnZXQiLCJ0aGVuIiwiZGF0YSIsImRhdGFGZXRjaCIsIk9iamVjdCIsImtleXMiLCJtZXRhIiwiZGVzY3JpcHRpb24iLCJob3VycyIsImZpbHRlciIsIm5vdyIsInllYXIiLCJtb250aCIsImVhY2giLCJpbmNsdWRlcyIsImVhY2hvbmUiLCJzbGljZSIsImVhY2h0d28iLCJrZXl0d28iLCJwcmljZSIsInByaWNlVmFsdWUiLCJkYXRlIiwiX2hvdXJzIiwidmFsdWUiLCJrZXkiLCJuZXh0RGF5IiwidXRjIiwiYWRkIiwiY3VycmVudERhdGUiLCJtaW51dGUiLCJzZWNvbmRzIiwibWlsbGlzZWNvbmQiLCJleHBpcmUiLCJkaWZmIiwidmFsaWQiLCJrIiwicGFyc2VJbnQiLCJmb3JtYXQiLCJzcGxpdCIsInBvc2l0aXZlIiwiZGF0ZVRvU2F2ZSIsImFicyIsInN1YnRyYWN0IiwidG9TYXZlIiwiSlNPTiIsInN0cmluZ2lmeSIsInRvU2F2ZVN0cmluZyIsImV4aXN0cyIsInJlcGx5Iiwic2V0IiwiT25lWWVhckFnbyIsIm1vbnRocyIsImluZGV4Iiwib2JqIiwiYXNzaWduIiwiY3VycmVuY3lFeHBpcmUiLCJyZXZlcnNlIiwibmV4dE1vbnRoIiwicm91bmQiLCJMYXN0RGF5IiwicmFuZ2UiLCJzb21lIiwiY29pbiIsInByb21pc2UiLCJBcnJheSIsImtleURhdGUiLCJQcm9taXNlIiwicmVzb2x2ZSIsInJlamVjdCIsInBhcnNlIiwicHVzaCIsImFsbCIsIm1lc3NhZ2UiLCJMYXN0WWVhciJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7O0FBQUE7Ozs7QUFFQTs7OztBQUVBOzs7O0FBRUE7Ozs7QUFFQTs7Ozs7Ozs7OztBQUVBLElBQU1BLE1BQU0sa0JBQVo7O0FBRUEsSUFBTUMsV0FBVyxDQUFDLEtBQUQsRUFBUSxLQUFSLENBQWpCOztBQUVBLElBQU1DLFNBQVMsZ0JBQU1DLFlBQU4sRUFBZjs7QUFFQUQsT0FBT0UsRUFBUCxDQUFVLE9BQVYsRUFBbUIsVUFBQ0MsR0FBRCxFQUFTO0FBQ3hCQyxZQUFRQyxHQUFSLENBQVksV0FBV0YsR0FBdkI7QUFDSCxDQUZEOztBQUlBLHVCQUFTRyxXQUFULENBQXFCLGlCQUFyQixFQUF3QyxhQUF4QywwREFBdUQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQy9DQyx5QkFEK0M7O0FBQUE7QUFHL0NBLDRCQUFRQyxLQUFLQyxNQUFMLENBQVksQ0FBWixFQUFlLENBQWYsSUFBb0IsR0FBNUI7O0FBSCtDLHlCQUkzQ0YsS0FKMkM7QUFBQTtBQUFBO0FBQUE7O0FBSzNDSCw0QkFBUUMsR0FBUixDQUFZLGtDQUFaO0FBTDJDO0FBQUEsMkJBTXJDSyxpQkFOcUM7O0FBQUE7QUFBQSx3QkFRMUMsQ0FBQ0gsS0FSeUM7QUFBQTtBQUFBO0FBQUE7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsQ0FBdkQ7O0FBV0EsdUJBQVNELFdBQVQsQ0FBcUIsa0JBQXJCLEVBQXlDLGFBQXpDLDBEQUF3RDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFaERDLHlCQUZnRDs7QUFBQTtBQUloREEsNEJBQVFDLEtBQUtDLE1BQUwsQ0FBWSxDQUFaLEVBQWUsQ0FBZixJQUFvQixHQUE1Qjs7QUFKZ0QseUJBSzVDRixLQUw0QztBQUFBO0FBQUE7QUFBQTs7QUFNNUNILDRCQUFRQyxHQUFSLENBQVksbUNBQVo7QUFONEM7QUFBQSwyQkFPdENNLGtCQVBzQzs7QUFBQTtBQUFBO0FBQUE7O0FBQUE7QUFTNUNQLDRCQUFRQyxHQUFSLENBQVksMkJBQVo7O0FBVDRDO0FBQUEsd0JBWTNDLENBQUNFLEtBWjBDO0FBQUE7QUFBQTtBQUFBOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLENBQXhEOztBQWVBLElBQU1HO0FBQUEsd0VBQWtCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDcEJYLGlDQUFTYSxHQUFUO0FBQUEsZ0dBQWEsa0JBQU1DLFFBQU47QUFBQTs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSUNDLG1EQUpELG9GQUlzRkQsUUFKdEYsMkJBSW9IZixHQUpwSDtBQUFBO0FBQUEsdURBTWMsZ0JBQ2RpQixHQURjLENBQ1ZELEdBRFUsRUFFZEUsSUFGYyxDQUVULGlCQUFZO0FBQUEsd0RBQVZDLElBQVUsU0FBVkEsSUFBVTs7QUFDZCx3REFBTUMsWUFBWUQsSUFBbEI7O0FBRGMsdUVBR0tFLE9BQU9DLElBQVAsQ0FBWUYsU0FBWixDQUhMO0FBQUE7QUFBQSx3REFFUEcsSUFGTztBQUFBLHdEQUdWQyxXQUhVOztBQUlkLHdEQUFJLENBQUNBLFdBQUwsRUFBa0I7QUFDZGxCLGdFQUFRQyxHQUFSLENBQVksRUFBQ1Esa0JBQUQsRUFBV04sT0FBTyxJQUFsQixFQUFaO0FBQ0EsK0RBQU8sS0FBUDtBQUNIO0FBQ0Qsd0RBQU1nQixRQUFRSixPQUNUQyxJQURTLENBQ0pGLFVBQVVJLFdBQVYsQ0FESSxFQUVURSxNQUZTLENBRUYsZ0JBQVE7QUFDWiw0REFBSUMsTUFBSyx1QkFBVDtBQUNBLDREQUFJQyxPQUFLRCxJQUFJVixHQUFKLENBQVEsTUFBUixDQUFUO0FBQUEsNERBQ0lZLFFBQU1GLElBQUlWLEdBQUosQ0FBUSxPQUFSLElBQWlCLENBRDNCO0FBRUEsK0RBQU9hLEtBQUtDLFFBQUwsQ0FBaUJILElBQWpCLFNBQXlCQyxLQUF6QixDQUFQO0FBQ0gscURBUFMsRUFRVEgsTUFSUyxDQVFGO0FBQUEsK0RBQVdNLFFBQVFELFFBQVIsQ0FBaUIsT0FBakIsQ0FBWDtBQUFBLHFEQVJFLEVBU1RFLEtBVFMsQ0FTSCxDQVRHLEVBU0EsRUFUQSxFQVVUbkIsR0FWUyxDQVVMLFVBQUNvQixPQUFELEVBQVVDLE1BQVYsRUFBcUI7QUFBQSw0RUFDUmQsT0FBT0MsSUFBUCxDQUFZRixVQUFVSSxXQUFWLEVBQXVCVSxPQUF2QixDQUFaLENBRFE7QUFBQTtBQUFBLDREQUNqQkUsS0FEaUI7O0FBRXRCLDREQUFJQyxhQUFhakIsVUFBVUksV0FBVixFQUF1QlUsT0FBdkIsRUFBZ0NFLEtBQWhDLENBQWpCO0FBQ0EsK0RBQU8sRUFBQ0UsTUFBTUosT0FBUCxFQUFnQkUsT0FBT0MsVUFBdkIsRUFBbUN0QixrQkFBbkMsRUFBNkNvQixjQUE3QyxFQUFQO0FBQ0gscURBZFMsQ0FBZDtBQWVBLDJEQUFPVixLQUFQO0FBQ0gsaURBMUJjLENBTmQ7O0FBQUE7QUFNRGMsc0RBTkM7O0FBaUNMLG9EQUFJLENBQUNBLE1BQUwsRUFBYTtBQUNUakMsNERBQVFDLEdBQVIsQ0FBWSw4QkFBWjtBQUNBSztBQUNILGlEQUhELE1BR087QUFDSDJCLDJEQUFPekIsR0FBUCxDQUFXLFVBQUMwQixLQUFELEVBQVFDLEdBQVIsRUFBZ0I7O0FBRXZCLDREQUFJQyxVQUFVLGlCQUNUQyxHQURTLENBQ0xILE1BQU1GLElBREQsRUFFVE0sR0FGUyxDQUVMLENBRkssRUFFRixLQUZFLENBQWQ7O0FBSUEsNERBQUlDLGNBQWMsd0JBQ2JDLE1BRGEsQ0FDTixDQURNLEVBRWJDLE9BRmEsQ0FFTCxDQUZLLEVBR2JDLFdBSGEsQ0FHRCxDQUhDLENBQWxCOztBQUtBLDREQUFJQyxTQUFTUCxRQUFRUSxJQUFSLENBQWFMLFdBQWIsSUFBNEIsSUFBekM7QUFDQUwsOERBQU1TLE1BQU4sR0FBZUEsTUFBZjtBQUNBLCtEQUFPVCxLQUFQO0FBQ0gscURBZEQsRUFlS2QsTUFmTCxDQWVZO0FBQUEsK0RBQVN5QixLQUFUO0FBQUEscURBZlosRUFnQktyQyxHQWhCTDtBQUFBLDRIQWdCUyxrQkFBTTBCLEtBQU4sRUFBYVksQ0FBYjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVPRixnRkFGUCxHQUVjRyxTQUFTLHdCQUFTQyxNQUFULENBQWdCLEdBQWhCLEVBQXFCQyxLQUFyQixDQUEyQixHQUEzQixFQUFnQyxDQUFoQyxDQUFULENBRmQ7QUFHT0Msb0ZBSFAsR0FHa0JOLE9BQU8sQ0FIekI7QUFJT08sc0ZBSlAsR0FJcUJELFFBQUQsR0FDWCxzQkFBT2hCLE1BQU1GLElBQWIsRUFBbUJNLEdBQW5CLENBQXVCbEMsS0FBS2dELEdBQUwsQ0FBU1IsSUFBVCxDQUF2QixFQUF1QyxNQUF2QyxFQUErQ0ksTUFBL0MsQ0FBc0QscUJBQXRELENBRFcsR0FFWCxzQkFBT2QsTUFBTUYsSUFBYixFQUFtQnFCLFFBQW5CLENBQTRCakQsS0FBS2dELEdBQUwsQ0FBU1IsSUFBVCxDQUE1QixFQUE0QyxNQUE1QyxFQUFvREksTUFBcEQsQ0FBMkQscUJBQTNELENBTlQ7O0FBQUEsZ0ZBT1FkLE1BQU1KLEtBUGQ7QUFBQTtBQUFBO0FBQUE7O0FBUU85QixvRkFBUUMsR0FBUixDQUFZLEVBQUNpQyxZQUFELEVBQVo7QUFSUDtBQUFBOztBQUFBO0FBVVdvQixrRkFWWCxHQVVvQjtBQUNUeEIsdUZBQU9JLE1BQU1KLEtBREo7QUFFVEUsc0ZBQU1tQjtBQUZHLDZFQVZwQjtBQUFBO0FBQUEsbUZBY2dDSSxLQUFLQyxTQUFMLENBQWVGLE1BQWYsQ0FkaEM7O0FBQUE7QUFjV0csd0ZBZFg7O0FBZU83RCxtRkFBTzhELE1BQVAsQ0FBaUJqRCxRQUFqQixTQUE2QjBDLFVBQTdCLEVBQTJDLFVBQUNwRCxHQUFELEVBQU00RCxLQUFOLEVBQWdCO0FBQ3ZELG9GQUFJQSxVQUFVLENBQWQsRUFBaUI7QUFDYix3RkFBSXpCLE1BQU1TLE1BQU4sR0FBZSxDQUFuQixFQUFzQjtBQUNsQi9DLCtGQUFPZ0UsR0FBUCxDQUFjbkQsUUFBZCxTQUEwQjBDLFVBQTFCLEVBQXdDTSxZQUF4QyxFQUFzRCxJQUF0RCxFQUE0RHZCLE1BQU1TLE1BQWxFO0FBQ0g7QUFDSjtBQUNKLDZFQU5EOztBQWZQO0FBQUE7QUFBQTs7QUFBQTtBQUFBO0FBQUE7O0FBMkJHM0Msb0ZBQVFDLEdBQVIsMEJBQW1DUSxRQUFuQztBQUNBVCxvRkFBUUMsR0FBUixDQUFZLEVBQUNFLG1CQUFELEVBQVo7O0FBNUJIO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHlEQWhCVDs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQWlESDtBQXRGSTtBQUFBOztBQUFBO0FBQUE7QUFBQTs7QUF3RkxILHdEQUFRQyxHQUFSLENBQVksV0FBWjtBQUNBRCx3REFBUUMsR0FBUixDQUFZLEVBQUNFLG1CQUFELEVBQVo7O0FBekZLO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLDZCQUFiOztBQUFBO0FBQUE7QUFBQTtBQUFBOztBQURvQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxLQUFsQjs7QUFBQTtBQUFBO0FBQUE7QUFBQSxHQUFOOztBQWdHQSxJQUFNSTtBQUFBLHdFQUFtQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ3JCLDRCQUFJO0FBQ0FaLHFDQUFTYSxHQUFUO0FBQUEsb0dBQWEsa0JBQU1DLFFBQU47QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0hDLHVEQURHLG1GQUNpRkQsUUFEakY7QUFBQTtBQUFBLDJEQUVILGdCQUNERSxHQURDLENBQ0dELEdBREgsRUFFREUsSUFGQyxDQUVJLGlCQUFZO0FBQUEsNERBQVZDLElBQVUsU0FBVkEsSUFBVTs7QUFBQSw0RUFFS0UsT0FBT0MsSUFBUCxDQUFZSCxJQUFaLENBRkw7QUFBQTtBQUFBLDREQUVWSyxXQUZVOztBQUdkLDREQUFJMkMsYUFBYSx3QkFDWlIsUUFEWSxDQUNILENBREcsRUFDQSxNQURBLEVBRVpMLE1BRlksQ0FFTCxZQUZLLENBQWpCO0FBR0EsNERBQU1jLFNBQVMvQyxPQUNWQyxJQURVLENBQ0xILEtBQUtLLFdBQUwsQ0FESyxFQUVWRSxNQUZVLENBRUg7QUFBQSxtRUFBUUksT0FBT3FDLFVBQWY7QUFBQSx5REFGRyxFQUdWckQsR0FIVSxDQUdOLFVBQUNnQixJQUFELEVBQU91QyxLQUFQLEVBQWlCO0FBQUEsZ0ZBRUpoRCxPQUFPQyxJQUFQLENBQVlILEtBQUtLLFdBQUwsRUFBa0JNLElBQWxCLENBQVosQ0FGSTtBQUFBO0FBQUEsZ0VBRWJNLEtBRmE7O0FBR2xCLGdFQUFJQyxhQUFhbEIsS0FBS0ssV0FBTCxFQUFrQk0sSUFBbEIsRUFBd0JNLEtBQXhCLENBQWpCO0FBQ0EsZ0VBQUlrQyxNQUFNakQsT0FBT2tELE1BQVAsQ0FBYyxFQUFkLEVBQWtCO0FBQ3hCakMsc0VBQU1SLElBRGtCO0FBRXhCTSx1RUFBT0M7QUFGaUIsNkRBQWxCLENBQVY7QUFJQSxtRUFBT2lDLEdBQVA7QUFDSCx5REFaVSxDQUFmO0FBYUEsNERBQUlFLGlCQUFpQkosT0FDaEJLLE9BRGdCLEdBRWhCM0QsR0FGZ0IsQ0FFWixVQUFDMEIsS0FBRCxFQUFRQyxHQUFSLEVBQWdCO0FBQ2pCLGdFQUFJaUMsWUFBWSxpQkFDWC9CLEdBRFcsR0FFWEMsR0FGVyxDQUVQSCxNQUFNLENBRkMsRUFFRSxPQUZGLENBQWhCO0FBR0EsZ0VBQUlJLGNBQWMsaUJBQU9GLEdBQVAsRUFBbEI7QUFDQUgsa0VBQU1TLE1BQU4sR0FBZXZDLEtBQUtpRSxLQUFMLENBQVdELFVBQVV4QixJQUFWLENBQWVMLFdBQWYsS0FBK0IsT0FBTyxFQUFQLEdBQVksRUFBM0MsQ0FBWCxLQUE4RCxLQUFLLEVBQUwsR0FBVSxHQUF4RSxDQUFmO0FBQ0EsbUVBQU9MLEtBQVA7QUFDSCx5REFUZ0IsRUFVaEJkLE1BVmdCLENBVVQ7QUFBQSxtRUFBU3lCLEtBQVQ7QUFBQSx5REFWUyxFQVdoQnJDLEdBWGdCLENBV1osVUFBQzBCLEtBQUQsRUFBUVksQ0FBUixFQUFjO0FBQ2YsZ0VBQUk7QUFDQWxELHVFQUFPOEQsTUFBUCxDQUFpQmpELFFBQWpCLFNBQTZCLHNCQUFPeUIsTUFBTUYsSUFBYixFQUFtQmdCLE1BQW5CLENBQTBCLFNBQTFCLENBQTdCLEVBQXFFLFVBQUNqRCxHQUFELEVBQU00RCxLQUFOLEVBQWdCO0FBQ2pGLHdFQUFJQSxVQUFVLENBQWQsRUFBaUI7O0FBRWIsNEVBQUl6QixNQUFNUyxNQUFOLEdBQWUsQ0FBbkIsRUFBc0I7QUFDbEIvQyxtRkFBT2dFLEdBQVAsQ0FBY25ELFFBQWQsU0FBMEIsc0JBQU95QixNQUFNRixJQUFiLEVBQW1CZ0IsTUFBbkIsQ0FBMEIsU0FBMUIsQ0FBMUIsRUFBa0VPLEtBQUtDLFNBQUwsQ0FBZSxFQUFDMUIsT0FBT0ksTUFBTUosS0FBZCxFQUFxQkUsTUFBTUUsTUFBTUYsSUFBakMsRUFBZixDQUFsRSxFQUEwSCxJQUExSCxFQUFnSUUsTUFBTVMsTUFBdEk7QUFFSDtBQUNKO0FBQ0osaUVBUkQ7QUFTSCw2REFWRCxDQVVFLE9BQU94QyxLQUFQLEVBQWM7QUFDWkgsd0VBQVFDLEdBQVIsMEJBQW1DUSxRQUFuQztBQUNBVCx3RUFBUUMsR0FBUixDQUFZLEVBQUNFLFlBQUQsRUFBWjtBQUNIO0FBRUoseURBM0JnQixDQUFyQjtBQTRCSCxxREFqREMsQ0FGRzs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxpQ0FBYjs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQXFESCx5QkF0REQsQ0FzREUsT0FBT0EsS0FBUCxFQUFjO0FBQ1pILG9DQUFRQyxHQUFSLENBQVksWUFBWjtBQUNBRCxvQ0FBUUMsR0FBUixDQUFZLEVBQUNFLFlBQUQsRUFBWjtBQUNIOztBQTFEb0I7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsS0FBbkI7O0FBQUE7QUFBQTtBQUFBO0FBQUEsR0FBTjs7QUE4REEsSUFBTW1FO0FBQUEseUVBQVUsa0JBQU03RCxRQUFOLEVBQWdCOEQsS0FBaEI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsNkJBQ1I1RSxTQUFTNkUsSUFBVCxDQUFjLFVBQUNDLElBQUQ7QUFBQSxtQ0FBVUEsU0FBU2hFLFFBQW5CO0FBQUEseUJBQWQsQ0FEUTtBQUFBO0FBQUE7QUFBQTs7QUFHSmlFLCtCQUhJLEdBR00sRUFITjs7QUFJUixxREFBSUMsTUFBTUosS0FBTixFQUFhdkQsSUFBYixFQUFKLEdBQXlCUixHQUF6QjtBQUFBLGlHQUE2QixrQkFBTWdCLElBQU4sRUFBWVcsR0FBWjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFckJ5Qyx1REFGcUIsR0FFWCx3QkFDVHZCLFFBRFMsQ0FDQTdCLElBREEsRUFDTSxNQUROLEVBRVR3QixNQUZTLENBRUYscUJBRkUsQ0FGVztBQU1yQmQscURBTnFCLEdBTWIsSUFBSTJDLE9BQUosQ0FBWSxVQUFDQyxPQUFELEVBQVVDLE1BQVYsRUFBcUI7QUFDekNuRiwyREFBT2UsR0FBUCxDQUFjRixRQUFkLFNBQTBCbUUsT0FBMUIsRUFBcUMsVUFBQzdFLEdBQUQsRUFBTTRELEtBQU4sRUFBZ0I7O0FBRWpELDREQUFJLENBQUM1RCxHQUFMLEVBQVU7QUFDTixnRUFBSTRELEtBQUosRUFBVztBQUNQbUIsd0VBQVF2QixLQUFLeUIsS0FBTCxDQUFXckIsS0FBWCxDQUFSO0FBQ0gsNkRBRkQsTUFFTztBQUNIbUIsd0VBQVEsQ0FBUjtBQUNIO0FBQ0oseURBTkQsTUFNTztBQUNIQSxvRUFBUSxDQUFSO0FBQ0g7QUFFSixxREFaRDtBQWFILGlEQWRXLENBTmE7O0FBcUJ6Qkosd0RBQVFPLElBQVIsQ0FBYS9DLEtBQWI7O0FBckJ5QjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSw2QkFBN0I7O0FBQUE7QUFBQTtBQUFBO0FBQUE7O0FBSlE7QUFBQTtBQUFBO0FBQUEsK0JBNkJTMkMsUUFBUUssR0FBUixDQUFZUixPQUFaLENBN0JUOztBQUFBO0FBQUE7QUFBQTtBQUFBOztBQUFBO0FBaUNKUywrQkFqQ0ksR0FpQ00sa0JBakNOO0FBQUEsMERBbUNEQSxPQW5DQzs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxLQUFWOztBQUFBO0FBQUE7QUFBQTtBQUFBLEdBQU47O0FBd0NBLElBQU1DO0FBQUEseUVBQVcsbUJBQU0zRSxRQUFOLEVBQWdCOEQsS0FBaEI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsNkJBQ1Q1RSxTQUFTNkUsSUFBVCxDQUFjLFVBQUNDLElBQUQ7QUFBQSxtQ0FBVUEsU0FBU2hFLFFBQW5CO0FBQUEseUJBQWQsQ0FEUztBQUFBO0FBQUE7QUFBQTs7QUFHTGlFLCtCQUhLLEdBR0ssRUFITDs7QUFJVCxxREFBSUMsTUFBTUosS0FBTixFQUFhdkQsSUFBYixFQUFKLEdBQXlCUixHQUF6QjtBQUFBLGlHQUE2QixtQkFBTWdCLElBQU4sRUFBWVcsR0FBWjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFckJ5Qyx1REFGcUIsR0FFWCx3QkFDVHZCLFFBRFMsQ0FDQTdCLElBREEsRUFDTSxPQUROLEVBRVR3QixNQUZTLENBRUYsU0FGRSxDQUZXO0FBTXJCZCxxREFOcUIsR0FNYixJQUFJMkMsT0FBSixDQUFZLFVBQUNDLE9BQUQsRUFBVUMsTUFBVixFQUFxQjtBQUN6Q25GLDJEQUFPZSxHQUFQLENBQWNGLFFBQWQsU0FBMEJtRSxPQUExQixFQUFxQyxVQUFDN0UsR0FBRCxFQUFNNEQsS0FBTixFQUFnQjtBQUNqRCw0REFBSSxDQUFDNUQsR0FBTCxFQUFVO0FBQ04sZ0VBQUk0RCxLQUFKLEVBQVc7QUFDUG1CLHdFQUFRdkIsS0FBS3lCLEtBQUwsQ0FBV3JCLEtBQVgsQ0FBUjtBQUNILDZEQUZELE1BRU87QUFDSG1CLHdFQUFRLENBQVI7QUFDSDtBQUNKLHlEQU5ELE1BTU87QUFDSEEsb0VBQVEsQ0FBUjtBQUNIO0FBRUoscURBWEQ7QUFZSCxpREFiVyxDQU5hOztBQW9CekJKLHdEQUFRTyxJQUFSLENBQWEvQyxLQUFiOztBQXBCeUI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsNkJBQTdCOztBQUFBO0FBQUE7QUFBQTtBQUFBOztBQUpTO0FBQUE7QUFBQTtBQUFBLCtCQTRCUTJDLFFBQVFLLEdBQVIsQ0FBWVIsT0FBWixDQTVCUjs7QUFBQTtBQUFBO0FBQUE7QUFBQTs7QUFBQTtBQWdDTFMsK0JBaENLLEdBZ0NLLGtCQWhDTDtBQUFBLDJEQWtDRkEsT0FsQ0U7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsS0FBWDs7QUFBQTtBQUFBO0FBQUE7QUFBQSxHQUFOOztRQXVDUWIsTyxHQUFBQSxPO1FBQVNjLFEsR0FBQUEsUTtRQUFVN0UsZ0IsR0FBQUEsZ0I7UUFBa0JELGUsR0FBQUEsZTtRQUFpQlgsUSxHQUFBQSxRIiwiZmlsZSI6ImNyeXB0b1NlcnZpY2UuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgYXhpb3MgZnJvbSAnYXhpb3MnO1xuXG5pbXBvcnQgcmVkaXMgZnJvbSAncmVkaXMnO1xuXG5pbXBvcnQgbW9tZW50IGZyb20gJ21vbWVudCc7XG5cbmltcG9ydCBTY2hlZHVsZSBmcm9tICdub2RlLXNjaGVkdWxlJztcblxuaW1wb3J0IGJhYmVsUG9seWZpbGwgZnJvbSAnYmFiZWwtcG9seWZpbGwnO1xuXG5jb25zdCBLRVkgPSAnS0dMVkxQSFlLVjJIUDVTUic7XG5cbmNvbnN0IENVUlJFTkNZID0gW1wiQlRDXCIsIFwiRVRIXCJdO1xuXG5jb25zdCBjbGllbnQgPSByZWRpcy5jcmVhdGVDbGllbnQoKTtcblxuY2xpZW50Lm9uKFwiZXJyb3JcIiwgKGVycikgPT4ge1xuICAgIGNvbnNvbGUubG9nKFwiRXJyb3IgXCIgKyBlcnIpO1xufSk7XG5cblNjaGVkdWxlLnNjaGVkdWxlSm9iKCdMYXN0RGF5U2NoZWR1bGUnLCAnMCAwICogKiAqIConLCBhc3luYygpID0+IHtcbiAgICBsZXQgZXJyb3I7XG4gICAgZG8ge1xuICAgICAgICBlcnJvciA9IE1hdGgucmFuZG9tKDAsIDEpIDwgMC4xO1xuICAgICAgICBpZiAoZXJyb3IpIHtcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiU2ltdWxhdGUgU3VjY2VzcyBMYXN0RGF5U2NoZWR1bGVcIilcbiAgICAgICAgICAgIGF3YWl0IExhc3REYXlTY2hlZHVsZSgpO1xuICAgICAgICB9XG4gICAgfSB3aGlsZSAoIWVycm9yKTtcbn0pO1xuXG5TY2hlZHVsZS5zY2hlZHVsZUpvYignTGFzdFllYXJTY2hlZHVsZScsICcwIDAgMSAqICogKicsIGFzeW5jKCkgPT4ge1xuXG4gICAgbGV0IGVycm9yO1xuICAgIGRvIHtcbiAgICAgICAgZXJyb3IgPSBNYXRoLnJhbmRvbSgwLCAxKSA8IDAuMTtcbiAgICAgICAgaWYgKGVycm9yKSB7XG4gICAgICAgICAgICBjb25zb2xlLmxvZyhcIlNpbXVsYXRlIFN1Y2Nlc3MgTGFzdFllYXJTY2hlZHVsZVwiKVxuICAgICAgICAgICAgYXdhaXQgTGFzdFllYXJTY2hlZHVsZSgpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgY29uc29sZS5sb2coXCJpbnRlbnRhciBMYXN0WWVhclNjaGVkdWxlXCIpXG5cbiAgICAgICAgfVxuICAgIH0gd2hpbGUgKCFlcnJvcik7XG59KTtcblxuY29uc3QgTGFzdERheVNjaGVkdWxlID0gYXN5bmMoKSA9PiB7XG4gICAgQ1VSUkVOQ1kubWFwKGFzeW5jKGN1cnJlbmN5KSA9PiB7XG5cbiAgICAgICAgdHJ5IHtcblxuICAgICAgICAgICAgY29uc3QgdXJsID0gYGh0dHBzOi8vd3d3LmFscGhhdmFudGFnZS5jby9xdWVyeT9mdW5jdGlvbj1ESUdJVEFMX0NVUlJFTkNZX0lOVFJBREFZJnN5bWJvbD0ke2N1cnJlbmN5fSZtYXJrZXQ9VVNEJmFwaWtleT0ke0tFWX1gO1xuXG4gICAgICAgICAgICBsZXQgX2hvdXJzID0gYXdhaXQgYXhpb3NcbiAgICAgICAgICAgICAgICAuZ2V0KHVybClcbiAgICAgICAgICAgICAgICAudGhlbigoe2RhdGF9KSA9PiB7XG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGRhdGFGZXRjaCA9IGRhdGE7XG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IFttZXRhLFxuICAgICAgICAgICAgICAgICAgICAgICAgZGVzY3JpcHRpb25dID0gT2JqZWN0LmtleXMoZGF0YUZldGNoKTtcbiAgICAgICAgICAgICAgICAgICAgaWYgKCFkZXNjcmlwdGlvbikge1xuICAgICAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coe2N1cnJlbmN5LCBlcnJvcjogdHJ1ZX0pO1xuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGhvdXJzID0gT2JqZWN0XG4gICAgICAgICAgICAgICAgICAgICAgICAua2V5cyhkYXRhRmV0Y2hbZGVzY3JpcHRpb25dKVxuICAgICAgICAgICAgICAgICAgICAgICAgLmZpbHRlcihlYWNoID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBsZXQgbm93ID1tb21lbnQoKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBsZXQgeWVhcj1ub3cuZ2V0KCd5ZWFyJyksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG1vbnRoPW5vdy5nZXQoJ21vbnRoJykrMTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gZWFjaC5pbmNsdWRlcyhgJHt5ZWFyfS0ke21vbnRofWApXG4gICAgICAgICAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgICAgICAgICAgLmZpbHRlcihlYWNob25lID0+IGVhY2hvbmUuaW5jbHVkZXMoXCIwMDowMFwiKSlcbiAgICAgICAgICAgICAgICAgICAgICAgIC5zbGljZSgwLCAyNClcbiAgICAgICAgICAgICAgICAgICAgICAgIC5tYXAoKGVhY2h0d28sIGtleXR3bykgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxldCBbcHJpY2VdID0gT2JqZWN0LmtleXMoZGF0YUZldGNoW2Rlc2NyaXB0aW9uXVtlYWNodHdvXSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbGV0IHByaWNlVmFsdWUgPSBkYXRhRmV0Y2hbZGVzY3JpcHRpb25dW2VhY2h0d29dW3ByaWNlXTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4ge2RhdGU6IGVhY2h0d28sIHByaWNlOiBwcmljZVZhbHVlLCBjdXJyZW5jeSwga2V5dHdvfVxuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBob3VycztcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIGlmICghX2hvdXJzKSB7XG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2coXCJub3RoaW5nIHRvIGRvIC0gcmVwZWF0IEZldGNoXCIpO1xuICAgICAgICAgICAgICAgIExhc3REYXlTY2hlZHVsZSgpO1xuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICBfaG91cnMubWFwKCh2YWx1ZSwga2V5KSA9PiB7XG5cbiAgICAgICAgICAgICAgICAgICAgbGV0IG5leHREYXkgPSBtb21lbnRcbiAgICAgICAgICAgICAgICAgICAgICAgIC51dGModmFsdWUuZGF0ZSlcbiAgICAgICAgICAgICAgICAgICAgICAgIC5hZGQoMSwgJ2RheScpO1xuXG4gICAgICAgICAgICAgICAgICAgIGxldCBjdXJyZW50RGF0ZSA9IG1vbWVudCgpXG4gICAgICAgICAgICAgICAgICAgICAgICAubWludXRlKDApXG4gICAgICAgICAgICAgICAgICAgICAgICAuc2Vjb25kcygwKVxuICAgICAgICAgICAgICAgICAgICAgICAgLm1pbGxpc2Vjb25kKDApO1xuXG4gICAgICAgICAgICAgICAgICAgIGxldCBleHBpcmUgPSBuZXh0RGF5LmRpZmYoY3VycmVudERhdGUpIC8gMTAwMDtcbiAgICAgICAgICAgICAgICAgICAgdmFsdWUuZXhwaXJlID0gZXhwaXJlO1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gdmFsdWU7XG4gICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICAgICAgLmZpbHRlcih2YWxpZCA9PiB2YWxpZClcbiAgICAgICAgICAgICAgICAgICAgLm1hcChhc3luYyh2YWx1ZSwgaykgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgdHJ5IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBsZXQgZGlmZiA9IHBhcnNlSW50KG1vbWVudCgpLmZvcm1hdChcIlpcIikuc3BsaXQoJzonKVswXSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbGV0IHBvc2l0aXZlID0gZGlmZiA+IDA7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbGV0IGRhdGVUb1NhdmUgPSAocG9zaXRpdmUpXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgID8gbW9tZW50KHZhbHVlLmRhdGUpLmFkZChNYXRoLmFicyhkaWZmKSwgJ2hvdXInKS5mb3JtYXQoXCJZWVlZLU1NLUREIEhIOm1tOnNzXCIpXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDogbW9tZW50KHZhbHVlLmRhdGUpLnN1YnRyYWN0KE1hdGguYWJzKGRpZmYpLCAnaG91cicpLmZvcm1hdChcIllZWVktTU0tREQgSEg6bW06c3NcIik7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKCF2YWx1ZS5wcmljZSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyh7dmFsdWV9KVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxldCB0b1NhdmUgPSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBwcmljZTogdmFsdWUucHJpY2UsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkYXRlOiBkYXRlVG9TYXZlXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH07XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxldCB0b1NhdmVTdHJpbmcgPSBhd2FpdCBKU09OLnN0cmluZ2lmeSh0b1NhdmUpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjbGllbnQuZXhpc3RzKGAke2N1cnJlbmN5fS0ke2RhdGVUb1NhdmV9YCwgKGVyciwgcmVwbHkpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChyZXBseSA9PT0gMCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmICh2YWx1ZS5leHBpcmUgPiAwKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNsaWVudC5zZXQoYCR7Y3VycmVuY3l9LSR7ZGF0ZVRvU2F2ZX1gLCB0b1NhdmVTdHJpbmcsICdFWCcsIHZhbHVlLmV4cGlyZSlcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xuICAgIFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cblxuXG4gICAgICAgICAgICAgICAgICAgICAgICB9IGNhdGNoIChlcnJvcikge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKGBFcnJvciBlbiBlbCBFeGlzdCAtICR7Y3VycmVuY3l9YCk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coe2Vycm9yfSk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgIH1cbiAgICAgICAgfSBjYXRjaCAoZXJyb3IpIHtcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiRXJyb3IgRGF5XCIpO1xuICAgICAgICAgICAgY29uc29sZS5sb2coe2Vycm9yfSlcbiAgICAgICAgfVxuXG4gICAgfSk7XG59O1xuXG5jb25zdCBMYXN0WWVhclNjaGVkdWxlID0gYXN5bmMoKSA9PiB7XG4gICAgdHJ5IHtcbiAgICAgICAgQ1VSUkVOQ1kubWFwKGFzeW5jKGN1cnJlbmN5KSA9PiB7XG4gICAgICAgICAgICBjb25zdCB1cmwgPSBgaHR0cHM6Ly93d3cuYWxwaGF2YW50YWdlLmNvL3F1ZXJ5P2Z1bmN0aW9uPURJR0lUQUxfQ1VSUkVOQ1lfTU9OVEhMWSZzeW1ib2w9JHtjdXJyZW5jeX0mbWFya2V0PVVTRCZhcGlrZXk9S0dMVkxQSFlLVjJIUDVTUmA7XG4gICAgICAgICAgICBhd2FpdCBheGlvc1xuICAgICAgICAgICAgICAgIC5nZXQodXJsKVxuICAgICAgICAgICAgICAgIC50aGVuKCh7ZGF0YX0pID0+IHtcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgWyxcbiAgICAgICAgICAgICAgICAgICAgICAgIGRlc2NyaXB0aW9uXSA9IE9iamVjdC5rZXlzKGRhdGEpO1xuICAgICAgICAgICAgICAgICAgICBsZXQgT25lWWVhckFnbyA9IG1vbWVudCgpXG4gICAgICAgICAgICAgICAgICAgICAgICAuc3VidHJhY3QoMSwgJ3llYXInKVxuICAgICAgICAgICAgICAgICAgICAgICAgLmZvcm1hdChcIllZWVktTU0tRERcIik7XG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IG1vbnRocyA9IE9iamVjdFxuICAgICAgICAgICAgICAgICAgICAgICAgLmtleXMoZGF0YVtkZXNjcmlwdGlvbl0pXG4gICAgICAgICAgICAgICAgICAgICAgICAuZmlsdGVyKGVhY2ggPT4gZWFjaCA+IE9uZVllYXJBZ28pXG4gICAgICAgICAgICAgICAgICAgICAgICAubWFwKChlYWNoLCBpbmRleCkgPT4ge1xuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbGV0IFtwcmljZV0gPSBPYmplY3Qua2V5cyhkYXRhW2Rlc2NyaXB0aW9uXVtlYWNoXSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbGV0IHByaWNlVmFsdWUgPSBkYXRhW2Rlc2NyaXB0aW9uXVtlYWNoXVtwcmljZV07XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbGV0IG9iaiA9IE9iamVjdC5hc3NpZ24oe30sIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGF0ZTogZWFjaCxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcHJpY2U6IHByaWNlVmFsdWVcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gb2JqXG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICAgICAgbGV0IGN1cnJlbmN5RXhwaXJlID0gbW9udGhzXG4gICAgICAgICAgICAgICAgICAgICAgICAucmV2ZXJzZSgpXG4gICAgICAgICAgICAgICAgICAgICAgICAubWFwKCh2YWx1ZSwga2V5KSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbGV0IG5leHRNb250aCA9IG1vbWVudFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAudXRjKClcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLmFkZChrZXkgKyAxLCAnbW9udGgnKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBsZXQgY3VycmVudERhdGUgPSBtb21lbnQudXRjKCk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWUuZXhwaXJlID0gTWF0aC5yb3VuZChuZXh0TW9udGguZGlmZihjdXJyZW50RGF0ZSkgLyAoMTAwMCAqIDYwICogNjApKSAqICg2MCAqIDYwICogNzI4KTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gdmFsdWU7XG4gICAgICAgICAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgICAgICAgICAgLmZpbHRlcih2YWxpZCA9PiB2YWxpZClcbiAgICAgICAgICAgICAgICAgICAgICAgIC5tYXAoKHZhbHVlLCBrKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdHJ5IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY2xpZW50LmV4aXN0cyhgJHtjdXJyZW5jeX0tJHttb21lbnQodmFsdWUuZGF0ZSkuZm9ybWF0KFwiWVlZWS1NTVwiKX1gLCAoZXJyLCByZXBseSkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHJlcGx5ID09PSAwKSB7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAodmFsdWUuZXhwaXJlID4gMCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjbGllbnQuc2V0KGAke2N1cnJlbmN5fS0ke21vbWVudCh2YWx1ZS5kYXRlKS5mb3JtYXQoXCJZWVlZLU1NXCIpfWAsIEpTT04uc3RyaW5naWZ5KHtwcmljZTogdmFsdWUucHJpY2UsIGRhdGU6IHZhbHVlLmRhdGV9KSwgJ0VYJywgdmFsdWUuZXhwaXJlKVxuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9IGNhdGNoIChlcnJvcikge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhgRXJyb3IgZW4gZWwgRXhpc3QgLSAke2N1cnJlbmN5fWApO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyh7ZXJyb3J9KTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICB9KTtcbiAgICB9IGNhdGNoIChlcnJvcikge1xuICAgICAgICBjb25zb2xlLmxvZyhcIkVycm9yIFllYXJcIik7XG4gICAgICAgIGNvbnNvbGUubG9nKHtlcnJvcn0pO1xuICAgIH1cblxufTtcblxuY29uc3QgTGFzdERheSA9IGFzeW5jKGN1cnJlbmN5LCByYW5nZSkgPT4ge1xuICAgIGlmIChDVVJSRU5DWS5zb21lKChjb2luKSA9PiBjb2luID09PSBjdXJyZW5jeSkpIHtcblxuICAgICAgICBsZXQgcHJvbWlzZSA9IFtdO1xuICAgICAgICBbLi4uQXJyYXkocmFuZ2UpLmtleXMoKV0ubWFwKGFzeW5jKGVhY2gsIGtleSkgPT4ge1xuXG4gICAgICAgICAgICBsZXQga2V5RGF0ZSA9IG1vbWVudCgpXG4gICAgICAgICAgICAgICAgLnN1YnRyYWN0KGVhY2gsICdob3VyJylcbiAgICAgICAgICAgICAgICAuZm9ybWF0KFwiWVlZWS1NTS1ERCBISDowMDowMFwiKTtcblxuICAgICAgICAgICAgbGV0IHZhbHVlID0gbmV3IFByb21pc2UoKHJlc29sdmUsIHJlamVjdCkgPT4ge1xuICAgICAgICAgICAgICAgIGNsaWVudC5nZXQoYCR7Y3VycmVuY3l9LSR7a2V5RGF0ZX1gLCAoZXJyLCByZXBseSkgPT4ge1xuXG4gICAgICAgICAgICAgICAgICAgIGlmICghZXJyKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAocmVwbHkpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXNvbHZlKEpTT04ucGFyc2UocmVwbHkpKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcmVzb2x2ZSgwKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlc29sdmUoMCk7XG4gICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICBwcm9taXNlLnB1c2godmFsdWUpO1xuXG4gICAgICAgIH0pO1xuXG4gICAgICAgIHJldHVybiBbLi4uYXdhaXQgUHJvbWlzZS5hbGwocHJvbWlzZSldO1xuXG4gICAgfSBlbHNlIHtcblxuICAgICAgICBsZXQgbWVzc2FnZSA9IFwiQ3VycmVuY3kgSW52YWxpZFwiO1xuXG4gICAgICAgIHJldHVybiBtZXNzYWdlO1xuICAgIH1cblxufTtcblxuY29uc3QgTGFzdFllYXIgPSBhc3luYyhjdXJyZW5jeSwgcmFuZ2UpID0+IHtcbiAgICBpZiAoQ1VSUkVOQ1kuc29tZSgoY29pbikgPT4gY29pbiA9PT0gY3VycmVuY3kpKSB7XG5cbiAgICAgICAgbGV0IHByb21pc2UgPSBbXTtcbiAgICAgICAgWy4uLkFycmF5KHJhbmdlKS5rZXlzKCldLm1hcChhc3luYyhlYWNoLCBrZXkpID0+IHtcblxuICAgICAgICAgICAgbGV0IGtleURhdGUgPSBtb21lbnQoKVxuICAgICAgICAgICAgICAgIC5zdWJ0cmFjdChlYWNoLCAnbW9udGgnKVxuICAgICAgICAgICAgICAgIC5mb3JtYXQoXCJZWVlZLU1NXCIpO1xuXG4gICAgICAgICAgICBsZXQgdmFsdWUgPSBuZXcgUHJvbWlzZSgocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XG4gICAgICAgICAgICAgICAgY2xpZW50LmdldChgJHtjdXJyZW5jeX0tJHtrZXlEYXRlfWAsIChlcnIsIHJlcGx5KSA9PiB7XG4gICAgICAgICAgICAgICAgICAgIGlmICghZXJyKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAocmVwbHkpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXNvbHZlKEpTT04ucGFyc2UocmVwbHkpKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcmVzb2x2ZSgwKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlc29sdmUoMCk7XG4gICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICBwcm9taXNlLnB1c2godmFsdWUpO1xuXG4gICAgICAgIH0pO1xuXG4gICAgICAgIHJldHVybiBbLi4uYXdhaXQgUHJvbWlzZS5hbGwocHJvbWlzZSldO1xuXG4gICAgfSBlbHNlIHtcblxuICAgICAgICBsZXQgbWVzc2FnZSA9IFwiQ3VycmVuY3kgSW52YWxpZFwiO1xuXG4gICAgICAgIHJldHVybiBtZXNzYWdlO1xuICAgIH1cblxufTtcblxuZXhwb3J0IHtMYXN0RGF5LCBMYXN0WWVhciwgTGFzdFllYXJTY2hlZHVsZSwgTGFzdERheVNjaGVkdWxlLCBDVVJSRU5DWX07XG4iXX0=