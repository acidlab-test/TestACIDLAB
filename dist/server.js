'use strict';

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _helmet = require('helmet');

var _helmet2 = _interopRequireDefault(_helmet);

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var _http = require('http');

var _cors = require('cors');

var _cors2 = _interopRequireDefault(_cors);

var _moment = require('moment');

var _moment2 = _interopRequireDefault(_moment);

var _bodyParser = require('body-parser');

var _bodyParser2 = _interopRequireDefault(_bodyParser);

var _babelPolyfill = require('babel-polyfill');

var _babelPolyfill2 = _interopRequireDefault(_babelPolyfill);

var _axios = require('axios');

var _axios2 = _interopRequireDefault(_axios);

var _cryptoService = require('./cryptoService');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

var app = (0, _express2.default)();
var PORT = 8000;
var server = new _http.Server(app);

app.use(_express2.default.static(_path2.default.join(__dirname, '../public')));
app.use((0, _helmet2.default)());
app.use((0, _cors2.default)());
app.use(_bodyParser2.default.urlencoded({ extended: true }));

app.use(_bodyParser2.default.json());

app.get('/chart', function (req, res) {
  return res.redirect('/');
});

app.get('/api/:currency/lastday', function () {
  var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(req, res) {
    var currency, _result;

    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            currency = req.params.currency.toUpperCase();

            if (!_cryptoService.CURRENCY.some(function (each) {
              return each === currency;
            })) {
              _context.next = 6;
              break;
            }

            _context.next = 4;
            return (0, _cryptoService.LastDay)(currency, 24);

          case 4:
            _result = _context.sent;
            return _context.abrupt('return', res.json({ currency: currency, data: _result }));

          case 6:

            res.json("currency invalid - only BTC, ETH");

          case 7:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, undefined);
  }));

  return function (_x, _x2) {
    return _ref.apply(this, arguments);
  };
}());

app.get('/api/:currency/lastyear', function () {
  var _ref2 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(req, res) {
    var currency, _result;

    return regeneratorRuntime.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            currency = req.params.currency.toUpperCase();

            if (!_cryptoService.CURRENCY.some(function (each) {
              return each === currency;
            })) {
              _context2.next = 6;
              break;
            }

            _context2.next = 4;
            return (0, _cryptoService.LastYear)(currency, 12);

          case 4:
            _result = _context2.sent;
            return _context2.abrupt('return', res.json({ currency: currency, data: _result }));

          case 6:
            res.json("currency invalid - only BTC, ETH");

          case 7:
          case 'end':
            return _context2.stop();
        }
      }
    }, _callee2, undefined);
  }));

  return function (_x3, _x4) {
    return _ref2.apply(this, arguments);
  };
}());

server.listen(PORT, function () {
  console.log('Server Running');
  setTimeout(_asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
    return regeneratorRuntime.wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            _context3.next = 2;
            return (0, _cryptoService.LastDaySchedule)();

          case 2:
            _context3.next = 4;
            return (0, _cryptoService.LastYearSchedule)();

          case 4:
          case 'end':
            return _context3.stop();
        }
      }
    }, _callee3, undefined);
  })), 5000);
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3NyYy9zZXJ2ZXIuanMiXSwibmFtZXMiOlsiYXBwIiwiUE9SVCIsInNlcnZlciIsInVzZSIsInN0YXRpYyIsImpvaW4iLCJfX2Rpcm5hbWUiLCJ1cmxlbmNvZGVkIiwiZXh0ZW5kZWQiLCJqc29uIiwiZ2V0IiwicmVxIiwicmVzIiwicmVkaXJlY3QiLCJjdXJyZW5jeSIsInBhcmFtcyIsInRvVXBwZXJDYXNlIiwic29tZSIsImVhY2giLCJfcmVzdWx0IiwiZGF0YSIsImxpc3RlbiIsImNvbnNvbGUiLCJsb2ciLCJzZXRUaW1lb3V0Il0sIm1hcHBpbmdzIjoiOztBQUFBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUNBOztBQUNBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFDQTs7Ozs7O0FBRUEsSUFBTUEsTUFBTSx3QkFBWjtBQUNBLElBQU1DLE9BQU8sSUFBYjtBQUNBLElBQU1DLFNBQVMsaUJBQVdGLEdBQVgsQ0FBZjs7QUFFQUEsSUFBSUcsR0FBSixDQUFRLGtCQUFRQyxNQUFSLENBQWUsZUFBS0MsSUFBTCxDQUFVQyxTQUFWLEVBQXFCLFdBQXJCLENBQWYsQ0FBUjtBQUNBTixJQUFJRyxHQUFKLENBQVEsdUJBQVI7QUFDQUgsSUFBSUcsR0FBSixDQUFRLHFCQUFSO0FBQ0FILElBQUlHLEdBQUosQ0FBUSxxQkFBV0ksVUFBWCxDQUFzQixFQUFDQyxVQUFVLElBQVgsRUFBdEIsQ0FBUjs7QUFFQVIsSUFBSUcsR0FBSixDQUFRLHFCQUFXTSxJQUFYLEVBQVI7O0FBRUFULElBQUlVLEdBQUosQ0FBUSxRQUFSLEVBQWlCLFVBQUNDLEdBQUQsRUFBS0MsR0FBTDtBQUFBLFNBQVlBLElBQUlDLFFBQUosQ0FBYSxHQUFiLENBQVo7QUFBQSxDQUFqQjs7QUFFQWIsSUFBSVUsR0FBSixDQUFRLHdCQUFSO0FBQUEscUVBQWtDLGlCQUFNQyxHQUFOLEVBQVdDLEdBQVg7QUFBQTs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUM1QkUsb0JBRDRCLEdBQ2pCSCxJQUNaSSxNQURZLENBRVpELFFBRlksQ0FHWkUsV0FIWSxFQURpQjs7QUFBQSxpQkFLNUIsd0JBQVNDLElBQVQsQ0FBYztBQUFBLHFCQUFRQyxTQUFTSixRQUFqQjtBQUFBLGFBQWQsQ0FMNEI7QUFBQTtBQUFBO0FBQUE7O0FBQUE7QUFBQSxtQkFNUiw0QkFBUUEsUUFBUixFQUFrQixFQUFsQixDQU5ROztBQUFBO0FBTXhCSyxtQkFOd0I7QUFBQSw2Q0FPdkJQLElBQUlILElBQUosQ0FBUyxFQUFDSyxVQUFVQSxRQUFYLEVBQXFCTSxNQUFNRCxPQUEzQixFQUFULENBUHVCOztBQUFBOztBQVVoQ1AsZ0JBQUlILElBQUosQ0FBUyxrQ0FBVDs7QUFWZ0M7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsR0FBbEM7O0FBQUE7QUFBQTtBQUFBO0FBQUE7O0FBYUFULElBQUlVLEdBQUosQ0FBUSx5QkFBUjtBQUFBLHNFQUFtQyxrQkFBTUMsR0FBTixFQUFXQyxHQUFYO0FBQUE7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDN0JFLG9CQUQ2QixHQUNsQkgsSUFDWkksTUFEWSxDQUVaRCxRQUZZLENBR1pFLFdBSFksRUFEa0I7O0FBQUEsaUJBSzdCLHdCQUFTQyxJQUFULENBQWM7QUFBQSxxQkFBUUMsU0FBU0osUUFBakI7QUFBQSxhQUFkLENBTDZCO0FBQUE7QUFBQTtBQUFBOztBQUFBO0FBQUEsbUJBTVQsNkJBQVNBLFFBQVQsRUFBbUIsRUFBbkIsQ0FOUzs7QUFBQTtBQU16QkssbUJBTnlCO0FBQUEsOENBT3hCUCxJQUFJSCxJQUFKLENBQVMsRUFBQ0ssVUFBVUEsUUFBWCxFQUFxQk0sTUFBTUQsT0FBM0IsRUFBVCxDQVB3Qjs7QUFBQTtBQVVqQ1AsZ0JBQUlILElBQUosQ0FBUyxrQ0FBVDs7QUFWaUM7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsR0FBbkM7O0FBQUE7QUFBQTtBQUFBO0FBQUE7O0FBY0FQLE9BQU9tQixNQUFQLENBQWNwQixJQUFkLEVBQW9CLFlBQU07QUFDeEJxQixVQUFRQyxHQUFSO0FBQ0FDLHFFQUFXO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLG1CQUNILHFDQURHOztBQUFBO0FBQUE7QUFBQSxtQkFFSCxzQ0FGRzs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxHQUFYLElBR0csSUFISDtBQUlELENBTkQiLCJmaWxlIjoic2VydmVyLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IEV4cHJlc3MgZnJvbSAnZXhwcmVzcyc7XG5pbXBvcnQgSGVsbWV0IGZyb20gJ2hlbG1ldCc7XG5pbXBvcnQgcGF0aCBmcm9tICdwYXRoJztcbmltcG9ydCB7U2VydmVyfSBmcm9tICdodHRwJztcbmltcG9ydCBDb3JzIGZyb20gJ2NvcnMnO1xuaW1wb3J0IG1vbWVudCBmcm9tICdtb21lbnQnO1xuaW1wb3J0IEJvZHlQYXJzZXIgZnJvbSAnYm9keS1wYXJzZXInO1xuaW1wb3J0IGJhYmVsUG9seWZpbGwgZnJvbSAnYmFiZWwtcG9seWZpbGwnO1xuaW1wb3J0IGF4aW9zIGZyb20gJ2F4aW9zJztcbmltcG9ydCB7TGFzdERheSwgTGFzdFllYXIsIExhc3RZZWFyU2NoZWR1bGUgYXMgTG9hZFllYXJJbml0aWFsLCBMYXN0RGF5U2NoZWR1bGUgYXMgTG9hZERheUluaXRpYWwsIENVUlJFTkNZfSBmcm9tICcuL2NyeXB0b1NlcnZpY2UnO1xuXG5jb25zdCBhcHAgPSBFeHByZXNzKCk7XG5jb25zdCBQT1JUID0gODAwMDtcbmNvbnN0IHNlcnZlciA9IG5ldyBTZXJ2ZXIoYXBwKTtcblxuYXBwLnVzZShFeHByZXNzLnN0YXRpYyhwYXRoLmpvaW4oX19kaXJuYW1lLCAnLi4vcHVibGljJykpKTtcbmFwcC51c2UoSGVsbWV0KCkpO1xuYXBwLnVzZShDb3JzKCkpO1xuYXBwLnVzZShCb2R5UGFyc2VyLnVybGVuY29kZWQoe2V4dGVuZGVkOiB0cnVlfSkpO1xuXG5hcHAudXNlKEJvZHlQYXJzZXIuanNvbigpKTtcblxuYXBwLmdldCgnL2NoYXJ0JywocmVxLHJlcyk9PiByZXMucmVkaXJlY3QoJy8nKSk7XG5cbmFwcC5nZXQoJy9hcGkvOmN1cnJlbmN5L2xhc3RkYXknLCBhc3luYyhyZXEsIHJlcykgPT4ge1xuICBsZXQgY3VycmVuY3kgPSByZXFcbiAgICAucGFyYW1zXG4gICAgLmN1cnJlbmN5XG4gICAgLnRvVXBwZXJDYXNlKCk7XG4gIGlmIChDVVJSRU5DWS5zb21lKGVhY2ggPT4gZWFjaCA9PT0gY3VycmVuY3kpKSB7XG4gICAgY29uc3QgX3Jlc3VsdCA9IGF3YWl0IExhc3REYXkoY3VycmVuY3ksIDI0KTtcbiAgICByZXR1cm4gcmVzLmpzb24oe2N1cnJlbmN5OiBjdXJyZW5jeSwgZGF0YTogX3Jlc3VsdH0pO1xuICB9XG5cbiAgcmVzLmpzb24oXCJjdXJyZW5jeSBpbnZhbGlkIC0gb25seSBCVEMsIEVUSFwiKTtcbn0pO1xuXG5hcHAuZ2V0KCcvYXBpLzpjdXJyZW5jeS9sYXN0eWVhcicsIGFzeW5jKHJlcSwgcmVzKSA9PiB7XG4gIGxldCBjdXJyZW5jeSA9IHJlcVxuICAgIC5wYXJhbXNcbiAgICAuY3VycmVuY3lcbiAgICAudG9VcHBlckNhc2UoKTtcbiAgaWYgKENVUlJFTkNZLnNvbWUoZWFjaCA9PiBlYWNoID09PSBjdXJyZW5jeSkpIHtcbiAgICBjb25zdCBfcmVzdWx0ID0gYXdhaXQgTGFzdFllYXIoY3VycmVuY3ksIDEyKTtcbiAgICByZXR1cm4gcmVzLmpzb24oe2N1cnJlbmN5OiBjdXJyZW5jeSwgZGF0YTogX3Jlc3VsdH0pO1xuXG4gIH1cbiAgcmVzLmpzb24oXCJjdXJyZW5jeSBpbnZhbGlkIC0gb25seSBCVEMsIEVUSFwiKTtcblxufSk7XG5cbnNlcnZlci5saXN0ZW4oUE9SVCwgKCkgPT4ge1xuICBjb25zb2xlLmxvZyhgU2VydmVyIFJ1bm5pbmdgKTtcbiAgc2V0VGltZW91dChhc3luYygpID0+IHtcbiAgICBhd2FpdCBMb2FkRGF5SW5pdGlhbCgpO1xuICAgIGF3YWl0IExvYWRZZWFySW5pdGlhbCgpO1xuICB9LCA1MDAwKTtcbn0pO1xuIl19