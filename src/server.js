import Express from 'express';
import Helmet from 'helmet';
import path from 'path';
import {Server} from 'http';
import Cors from 'cors';
import moment from 'moment';
import BodyParser from 'body-parser';
import babelPolyfill from 'babel-polyfill';
import axios from 'axios';
import {LastDay, LastYear, LastYearSchedule as LoadYearInitial, LastDaySchedule as LoadDayInitial, CURRENCY} from './cryptoService';

const app = Express();
const PORT = 8000;
const server = new Server(app);

app.use(Express.static(path.join(__dirname, '../public')));
app.use(Helmet());
app.use(Cors());
app.use(BodyParser.urlencoded({extended: true}));

app.use(BodyParser.json());

app.get('/chart',(req,res)=> res.redirect('/'));

app.get('/api/:currency/lastday', async(req, res) => {
  let currency = req
    .params
    .currency
    .toUpperCase();
  if (CURRENCY.some(each => each === currency)) {
    const _result = await LastDay(currency, 24);
    return res.json({currency: currency, data: _result});
  }

  res.json("currency invalid - only BTC, ETH");
});

app.get('/api/:currency/lastyear', async(req, res) => {
  let currency = req
    .params
    .currency
    .toUpperCase();
  if (CURRENCY.some(each => each === currency)) {
    const _result = await LastYear(currency, 12);
    return res.json({currency: currency, data: _result});

  }
  res.json("currency invalid - only BTC, ETH");

});

server.listen(PORT, () => {
  console.log(`Server Running`);
  setTimeout(async() => {
    await LoadDayInitial();
    await LoadYearInitial();
  }, 5000);
});
