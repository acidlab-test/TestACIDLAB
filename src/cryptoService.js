import axios from 'axios';

import redis from 'redis';

import moment from 'moment';

import Schedule from 'node-schedule';

import babelPolyfill from 'babel-polyfill';

const KEY = 'KGLVLPHYKV2HP5SR';

const CURRENCY = ["BTC", "ETH"];

const client = redis.createClient();

client.on("error", (err) => {
    console.log("Error " + err);
});

Schedule.scheduleJob('LastDaySchedule', '0 0 * * * *', async() => {
    let error;
    do {
        error = Math.random(0, 1) < 0.1;
        if (error) {
            console.log("Simulate Success LastDaySchedule")
            await LastDaySchedule();
        }
    } while (!error);
});

Schedule.scheduleJob('LastYearSchedule', '0 0 1 * * *', async() => {

    let error;
    do {
        error = Math.random(0, 1) < 0.1;
        if (error) {
            console.log("Simulate Success LastYearSchedule")
            await LastYearSchedule();
        } else {
            console.log("intentar LastYearSchedule")

        }
    } while (!error);
});

const LastDaySchedule = async() => {
    CURRENCY.map(async(currency) => {

        try {

            const url = `https://www.alphavantage.co/query?function=DIGITAL_CURRENCY_INTRADAY&symbol=${currency}&market=USD&apikey=${KEY}`;

            let _hours = await axios
                .get(url)
                .then(({data}) => {
                    const dataFetch = data;
                    const [meta,
                        description] = Object.keys(dataFetch);
                    if (!description) {
                        console.log({currency, error: true});
                        return false;
                    }
                    const hours = Object
                        .keys(dataFetch[description])
                        .filter(each => {
                            let now =moment();
                            let year=now.get('year'),
                                month=now.get('month')+1;
                            return each.includes(`${year}-${month}`)
                        })
                        .filter(eachone => eachone.includes("00:00"))
                        .slice(0, 24)
                        .map((eachtwo, keytwo) => {
                            let [price] = Object.keys(dataFetch[description][eachtwo]);
                            let priceValue = dataFetch[description][eachtwo][price];
                            return {date: eachtwo, price: priceValue, currency, keytwo}
                        });
                    return hours;
                });
            if (!_hours) {
                console.log("nothing to do - repeat Fetch");
                LastDaySchedule();
            } else {
                _hours.map((value, key) => {

                    let nextDay = moment
                        .utc(value.date)
                        .add(1, 'day');

                    let currentDate = moment()
                        .minute(0)
                        .seconds(0)
                        .millisecond(0);

                    let expire = nextDay.diff(currentDate) / 1000;
                    value.expire = expire;
                    return value;
                })
                    .filter(valid => valid)
                    .map(async(value, k) => {
                        try {
                            let diff = parseInt(moment().format("Z").split(':')[0]);
                            let positive = diff > 0;
                            let dateToSave = (positive)
                                ? moment(value.date).add(Math.abs(diff), 'hour').format("YYYY-MM-DD HH:mm:ss")
                                : moment(value.date).subtract(Math.abs(diff), 'hour').format("YYYY-MM-DD HH:mm:ss");
                            if (!value.price) {
                                console.log({value})
                            } else {
                                let toSave = {
                                    price: value.price,
                                    date: dateToSave
                                };
                                let toSaveString = await JSON.stringify(toSave);
                                client.exists(`${currency}-${dateToSave}`, (err, reply) => {
                                    if (reply === 0) {
                                        if (value.expire > 0) {
                                            client.set(`${currency}-${dateToSave}`, toSaveString, 'EX', value.expire)
                                        }
                                    }
                                });
    
                            }


                        } catch (error) {
                            console.log(`Error en el Exist - ${currency}`);
                            console.log({error});
                        }

                    });

            }
        } catch (error) {
            console.log("Error Day");
            console.log({error})
        }

    });
};

const LastYearSchedule = async() => {
    try {
        CURRENCY.map(async(currency) => {
            const url = `https://www.alphavantage.co/query?function=DIGITAL_CURRENCY_MONTHLY&symbol=${currency}&market=USD&apikey=KGLVLPHYKV2HP5SR`;
            await axios
                .get(url)
                .then(({data}) => {
                    const [,
                        description] = Object.keys(data);
                    let OneYearAgo = moment()
                        .subtract(1, 'year')
                        .format("YYYY-MM-DD");
                    const months = Object
                        .keys(data[description])
                        .filter(each => each > OneYearAgo)
                        .map((each, index) => {

                            let [price] = Object.keys(data[description][each]);
                            let priceValue = data[description][each][price];
                            let obj = Object.assign({}, {
                                date: each,
                                price: priceValue
                            });
                            return obj
                        });
                    let currencyExpire = months
                        .reverse()
                        .map((value, key) => {
                            let nextMonth = moment
                                .utc()
                                .add(key + 1, 'month');
                            let currentDate = moment.utc();
                            value.expire = Math.round(nextMonth.diff(currentDate) / (1000 * 60 * 60)) * (60 * 60 * 728);
                            return value;
                        })
                        .filter(valid => valid)
                        .map((value, k) => {
                            try {
                                client.exists(`${currency}-${moment(value.date).format("YYYY-MM")}`, (err, reply) => {
                                    if (reply === 0) {

                                        if (value.expire > 0) {
                                            client.set(`${currency}-${moment(value.date).format("YYYY-MM")}`, JSON.stringify({price: value.price, date: value.date}), 'EX', value.expire)

                                        }
                                    }
                                });
                            } catch (error) {
                                console.log(`Error en el Exist - ${currency}`);
                                console.log({error});
                            }

                        });
                });
        });
    } catch (error) {
        console.log("Error Year");
        console.log({error});
    }

};

const LastDay = async(currency, range) => {
    if (CURRENCY.some((coin) => coin === currency)) {

        let promise = [];
        [...Array(range).keys()].map(async(each, key) => {

            let keyDate = moment()
                .subtract(each, 'hour')
                .format("YYYY-MM-DD HH:00:00");

            let value = new Promise((resolve, reject) => {
                client.get(`${currency}-${keyDate}`, (err, reply) => {

                    if (!err) {
                        if (reply) {
                            resolve(JSON.parse(reply));
                        } else {
                            resolve(0);
                        }
                    } else {
                        resolve(0);
                    }

                });
            });
            promise.push(value);

        });

        return [...await Promise.all(promise)];

    } else {

        let message = "Currency Invalid";

        return message;
    }

};

const LastYear = async(currency, range) => {
    if (CURRENCY.some((coin) => coin === currency)) {

        let promise = [];
        [...Array(range).keys()].map(async(each, key) => {

            let keyDate = moment()
                .subtract(each, 'month')
                .format("YYYY-MM");

            let value = new Promise((resolve, reject) => {
                client.get(`${currency}-${keyDate}`, (err, reply) => {
                    if (!err) {
                        if (reply) {
                            resolve(JSON.parse(reply));
                        } else {
                            resolve(0);
                        }
                    } else {
                        resolve(0);
                    }

                });
            });
            promise.push(value);

        });

        return [...await Promise.all(promise)];

    } else {

        let message = "Currency Invalid";

        return message;
    }

};

export {LastDay, LastYear, LastYearSchedule, LastDaySchedule, CURRENCY};
